Dropzone.autoDiscover = false;

var Templates = {

    cache: {},

    getTemplate: function (type) {
        if (!(type in this.cache)) {
            this.cache[type] = Handlebars.compile($("#template-" + type).html());
        }
        return this.cache[type];
    },

    render: function (type, data) {
        $('#content-' + type).html(this.getTemplate(type)(data));
    },

    renderPartial: function (type, data) {
        return this.getTemplate(type)(data);
    },

    addComment: function (data) {
        $('#section-answer .answer-comments').append($(this.renderPartial('comment', data)));
        $('#no-comments').hide();
    }

};

var Search = {
    items: {}
};

var Navigation = {

    inputSearchTerm: $('#search-term'),

    init: function () {

        this.refresh();

        this.inputSearchTerm.typeahead({
                source: function (query, process) {
                    return $.get('/search/' + query, function (data) {

                        Search.items = {};

                        process(data.map(function (obj) {
                            Search.items[obj.title] = obj;
                            return obj.title;
                        }));
                    });
                }, afterSelect: function (item) {
                    $('#search-term').val('');
                    Navigation.show('answer', Search.items[item].uri);
                }
            }
        );

        $('#action-show-all').on('click', function (event) {
            event.preventDefault();
            $('#search-term').val('');
            Navigation.show('answers', '/answer');
        });

        $(document).on('click', 'a.action-answer', function () {
            Navigation.show('answer', $(this).data('url'));
        });
    },

    show: function (section, url) {
        $('section').each(function () {
            $(this).toggle($(this).attr('id') == 'section-' + section);
        });

        if (section === 'main') {
            this.refresh();
        } else {
            $.get(url, function (data) {
                Templates.render(section, data);

                if (section == 'answer') {

                    $.each(data.comments, function (idx, comment) {
                        Templates.addComment(comment);
                    });

                }
            });
        }

    },

    refresh: function () {
        $.each(['most-recent', 'most-searched'], function (idx, val) {
            $.get('/' + val, function (data) {
                Templates.render(val, data);
            });
        });
    }

};

var Uploader = {

    uploaders: {},

    init: function (id) {
        var uploader = new Dropzone('#' + id + '-dropzone');
        uploader.on('success', function (file, response) {
            $('#' + id + '-form').append('<input type="hidden" class="file" name="files[]" value="' + response.record.id + '"/>');
        });

        this.uploaders[id] = uploader;
    },

    get: function (id) {
        if (!(id in this.uploaders)) {
            this.init(id);
        }
        return this.uploaders[id];
    }

};

var Errors = {

    show: function (el, message) {
        el.parent('div').toggleClass('has-error', true);
        el.next('span').text(message).show();
    },

    reset: function (el) {
        el.parent('div').toggleClass('has-error', false);
        el.next('span').text('').hide();
    }

};

var Modals = {

    reset: function (id) {
        Uploader.get(id).removeAllFiles(true);
        Uploader.get(id).disable();
        $('#' + id + ' .form-control').each(function () {
            Errors.reset($(this));
        }).val('');
        $('#' + id + 'input.file').remove();
        $('#' + id + ' button.buttonSave').off('click');
    },

    init: function () {
        $('#create-new').on('hide.bs.modal', function () {
            Modals.reset('create-new');
        }).on('show.bs.modal', function () {
            Uploader.get('create-new').enable();
            $('#create-new button.buttonSave').on('click', function () {
                $.post('/create-new', $('#create-new-form').serialize(), function (data) {
                    $('.form-control').each(function () {
                        Errors.reset($(this));

                    });
                    if ('errors' in data) {
                        $.each(data.errors, function (k, v) {
                            Errors.show($('#create-new-' + k), v);
                        });
                    } else {
                        $('#create-new').modal('hide');
                        Modals.reset('create-new');
                        $('#after-new').modal('show');
                        $('#after-new-show').data('url', data.record.uri);
                    }
                }, 'json');
            });
        });

        $('#create-comment').on('hide.bs.modal', function () {
            Modals.reset('create-comment');
        }).on('show.bs.modal', function () {
            Uploader.get('create-comment').enable();
            $('#create-comment button.buttonSave').on('click', function () {
                $.post('/create-comment', $('#create-comment-form').serialize(), function (data) {
                    $('.form-control').each(function () {
                        Errors.reset($(this));
                    });
                    if ('errors' in data) {
                        $.each(data.errors, function (k, v) {
                            Errors.show($('#create-comment-' + k), v);
                        });
                    } else {
                        $('#create-comment').modal('hide');
                        Templates.addComment(data.record);
                    }
                }, 'json');
            });
        });

        $('#after-new-create').on('click', function () {
            $('#after-new').modal('hide');
            $('#create-new').modal('show');
        });

        $('#after-new-show').on('click', function () {
            $('#after-new').modal('hide');
            Navigation.show('answer', $(this).data('url'));
        });

        $(document).on("click", "#action-create-comment", function () {
            $('#create-comment').modal('show');
            $('#create-comment-answer').val($(this).data('answer'));
        });

    }
};


Navigation.init();
Modals.init();
