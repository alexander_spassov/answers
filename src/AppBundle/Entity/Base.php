<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class Base
{

    protected $id;
    protected $timestamp;

    /**
     * @return array
     */
    public function getAll()
    {
        $result = get_object_vars($this);

        if ($result['timestamp']) {
            $result['timestamp'] = $result['timestamp']->format('Y-m-d H:i:s');
        }

        $result['uri'] = sprintf(
            '/%s/%d',
            str_replace('appbundle\entity\\', '', strtolower(get_called_class())),
            $this->id
        );

        return $result;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->timestamp = new \DateTime();
    }

}