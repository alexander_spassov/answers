<?php

namespace AppBundle\Controller;

use AppBundle\Entity\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;

class FileController extends Controller
{


    /**
     * @param Request $request
     * @Route("/file-upload", name="file-upload")
     * @return JsonResponse
     */
    public function actionUpload(Request $request)
    {

        /* @var UploadedFile */
        $uploadedFile = $request->files->get('file');

        $file = new File();

        $file->setFile($uploadedFile);

        $validator = $this->get('validator');
        $errors = $validator->validate($file);

        $result = array();

        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $result['errors'][] = $error->getMessage();
            }
        } else {

            $em = $this->getDoctrine()->getManager();
            $em->persist($file);
            $em->flush();

            $file->upload();

            $result['record'] = $file->getAll();

        }

        return new JsonResponse($result);
    }

    /**
     * @param int $id
     * @Route("/file/{id}", name="file")
     * @return Response
     */
    public function actionFile($id)
    {

        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:File');

        $file = $repository->find($id);


        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type',$file->getType());
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $file->getName() . '";');

        $response->sendHeaders();

        $response->setContent(file_get_contents($file->getPath()));

        return $response;
    }

}
