<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Answer;
use AppBundle\Entity\File;
use AppBundle\Entity\Search;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

class AnswersController extends Controller
{

    /**
     * @Route("/most-recent", name="most-recent")
     */
    public function actionMostRecent()
    {
        $recordsRepo = $this->getDoctrine()
            ->getRepository('AppBundle:Answer');

        $records = $recordsRepo->createQueryBuilder('a')
            ->orderBy('a.timestamp', 'desc')
            ->getQuery()
            ->setMaxResults(10)
            ->getResult();

        $result = [];

        if ($records) {
            foreach ($records as $record) {
                /** @var \AppBundle\Entity\Answer $record */
                $result[] = $record->getAll();
            }
        }

        $response = new JsonResponse($result);

        return $response;
    }

    /**
     * @Route("/most-searched", name="most-searched")
     */
    public function actionMostSearched()
    {

        $searchRepo = $this->getDoctrine()
            ->getRepository('AppBundle:Search');

        $searches = $searchRepo->createQueryBuilder('t')
            ->select('t, count(t.id) AS HIDDEN myCount')
            ->addOrderBy('myCount', 'DESC')
            ->groupBy('t.answer')
            ->getQuery()
            ->setMaxResults(10)
            ->getResult();


        if ($searches) {

            $recordsRepo = $this->getDoctrine()
                ->getRepository('AppBundle:Answer');

            foreach ($searches as $search) {
                /** @var \AppBundle\Entity\Search $search */
                $result[] = $recordsRepo->find($search->getAnswer())->getAll();
            }
        }

        $response = new JsonResponse($result);

        return $response;
    }

    /**
     * @Route("/search/{query}", name="search")
     */
    public function actionSearch($query)
    {

        $recordsRepo = $this->getDoctrine()
            ->getRepository('AppBundle:Answer');

        $records = $recordsRepo->createQueryBuilder('a')
            ->where('a.title LIKE :query')
            ->setParameter('query', '%'.$query.'%')
            ->getQuery()
            ->getResult();

        $result = array();

        if ($records) {
            foreach ($records as $record) {
                /** @var \AppBundle\Entity\Answer $record */
                $result[] = $record->getAll();
            }
        }

        $response = new JsonResponse($result);

        return $response;
    }

    /**
     * @Route("/create-new", name="saveAnswer")
     */
    public function actionSave(Request $request)
    {

        $answer = new Answer();
        $answer->setContent($request->request->get('content'));
        $answer->setTitle($request->request->get('title'));

        $validator = $this->get('validator');
        $errors = $validator->validate($answer);
        $result = array();

        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $result['errors'][$error->getPropertyPath()] = $error->getMessage();
            }
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->persist($answer);
            $em->flush();

            $fileRepository = $this->getDoctrine()
                ->getRepository('AppBundle:File');

            foreach ((array)$request->request->get('files') as $fileID) {
                $file = $fileRepository->find($fileID);

                if ($file) {
                    $file->setRecordID($answer->getId());
                    $file->setBelongsTo(File::BELONGS_TO_ANSWSER);
                    $em->persist($file);
                }
            }

            $em->flush();

        }

        $result['record'] = $answer->getAll();


        $response = new JsonResponse($result);

        return $response;
    }

    /**
     * @param int $id
     * @Route("/answer/{id}", name="answer")
     * @return JsonResponse
     */
    public function actionAnswer($id)
    {

        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Answer');

        $answer = $repository->find($id);


        $em = $this->getDoctrine()->getManager();
        $search = new Search();
        $search->setAnswer($answer->getId());
        $em->persist($search);
        $em->flush();

        $result = $answer->getAll();
        $result['files'] = $this->getFiles($id, File::BELONGS_TO_ANSWSER);


        $result['comments'] = $this->getComments($id);


        $response = new JsonResponse($result);

        return $response;
    }

    /**
     * @param $id
     * @param $belongs_to
     * @return array
     */
    private function getFiles($id, $belongs_to)
    {

        $result = [];

        $fileRepository = $this->getDoctrine()->getRepository('AppBundle:File');

        $files = $fileRepository->findBy(
            [
                'record_id' => $id,
                'belongs_to' => $belongs_to,
            ]
        );

        if (!count($files)) {
            return $result;
        }

        foreach ($files as $file) {
            $result[] = $file->getAll();
        }


        return $result;
    }

    private function getComments($id)
    {
        $result = [];

        $commentRepository = $this->getDoctrine()->getRepository('AppBundle:Comment');

        $comments = $commentRepository->findBy(
            [
                'record_id' => $id,
            ]
        );


        if (!count($comments)) {
            return $result;
        }

        foreach ($comments as $comment) {
            $record = $comment->getAll();
            $record['files'] = $this->getFiles($comment->getId(), File::BELONGS_TO_COMMENT);
            $result[] = $record;
        }

        return $result;
    }

    /**
     * @Route("/answer", name="answers")
     * @return JsonResponse
     */
    public function actionAnswers()
    {

        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Answer');

        $answers = $repository->findAll();

        $result = [];

        if (count($answers)) {
            foreach ($answers as $answer) {
                $result[] = $answer->getAll();
            }
        }

        $response = new JsonResponse($result);

        return $response;
    }


}
