<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\File;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;

class CommentsController extends Controller
{

    /**
     * @Route("/create-comment", name="saveComment")
     */
    public function actionSave(Request $request)
    {

        $comment = new Comment();
        $comment->setContent($request->request->get('content'));
        $comment->setRecordID($request->request->get('answer'));

        $validator = $this->get('validator');
        $errors = $validator->validate($comment);
        $result = [];
        $files = [];

        if (count($errors) > 0) {
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $result['errors'][$error->getPropertyPath()] = $error->getMessage();
            }
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();

            $fileRepository = $this->getDoctrine()
                ->getRepository('AppBundle:File');

            foreach ((array) $request->request->get('files') as $fileID) {
                $file = $fileRepository->find($fileID);

                if ($file) {
                    $file->setRecordID($comment->getId());
                    $file->setBelongsTo(File::BELONGS_TO_COMMENT);
                    $em->persist($file);
                    $files[] = $file->getAll();
                }
            }

            $em->flush();

        }

        $result['record'] = $comment->getAll();
        $result['record']['files'] = $files;


        $response = new JsonResponse($result);

        return $response;
    }

}
