Installation
============

1. composer update

2. app/console doctrine:database:create

3. app/console doctrine:schema:update --force

4. app/console server:run
